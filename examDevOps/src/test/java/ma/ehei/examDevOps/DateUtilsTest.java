package ma.ehei.examDevOps;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Test;

public class DateUtilsTest {
    DateUtils dateUtils;

    @Before
    public void setUp()
	{
		dateUtils=new DateUtils();
	}
    
    @Test
    public void ajoutJoursSucces()
    {
        Date resultat=dateUtils.ajoutJours("11/04/2023", 5);
        Date recent= DateTime.parse("12/04/2023", DateTimeFormat.forPattern("dd/MM/yyyy")).toDate();
        assertEquals(resultat, recent);
    }

    @Test(resultat = Exception.class)
    public void ajoutJoursException()
    {
        dateUtils.ajoutJours("11/04/2023", -5);
    }
}
